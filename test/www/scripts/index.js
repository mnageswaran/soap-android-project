﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in cordova-simulate or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);
    
    function onDeviceReady() {

        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);

        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        var parentElement = document.getElementById('deviceready');

        //  var webServiceURL = 'http://www.holidaywebservice.com//HolidayService_v2/HolidayService2.asmx?wsdl';

        var webServiceURL = 'http://ussltccsw2049.solutions.glbsnet.com:7333/wsh/services/BatchServices/DataIntegration?WSDL';
        var username = '';
        //
        function CallService() {
            debugger;
             username = $('#email').val();
            var password = $('#password').val();

            var soapMessage = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsh="http://www.informatica.com/wsh">' +
                '<soapenv:Header/>' +
                '<soapenv:Body>' +
                '<wsh:Login>' +
                '<RepositoryDomainName>Domain_IMNC</RepositoryDomainName>' +
                '<RepositoryName>IMNC_PC_REP</RepositoryName>' +
                '<UserName>' + username + '</UserName>' +
                '<Password>' + password + '</Password>' +
                '<UserNameSpace>Native</UserNameSpace>' +
                '</wsh:Login>' +
                '</soapenv:Body>' +
                '</soapenv:Envelope>';
            if (username != '' & password != '') {
                $.ajax({
                    url: webServiceURL,
                    headers: {
                        'SOAPAction': '',
                        'Content-Type': 'text/xml'
                    },
                    type: "POST",
                    contentType: "text/xml",
                    dataType: "xml",
                    data: soapMessage,
                    success: OnSuccess,
                    error: OnError
                });
            }
            else {
                alert("Please enter username and password");
            }
            return false;
        }
        function OnError2(request, status, error) {
            debugger;
            alert('error occured');
     
            //alert('Username or Password is incorrect');
        }
        function OnSuccess2(data, status, req, xml, xmlHttpRequest, responseXML) {

            document.getElementById('main_login').style.display = "none";
                    document.getElementById('main_content').style.display = "inline-block";
                    var WorkflowRunStatus = $(req.responseText).find('WorkflowRunStatus').text();
                    document.getElementById('h_messasge').innerHTML = '<h4>' + WorkflowRunStatus + '</h2>' +
                        '<h4>Start Time: ' + $(req.responseText).find('StartTime').find('Date').text() + '/' + $(req.responseText).find('StartTime').find('Month').text() + '/' + $(req.responseText).find('StartTime').find('Year').text() + '</h2>' +
                        '<h4>End Time: ' + $(req.responseText).find('EndTime').find('Date').text() + '/' + $(req.responseText).find('StartTime').find('Month').text() + '/' + $(req.responseText).find('StartTime').find('Year').text() + '</h2>';
            
            //debugger;
            //$(req.responseText)
            //    .find('WorkflowRunStatus')
            //    .each(function () {

            //        debugger;
            //        document.getElementById('main_login').style.display = "none";
            //        document.getElementById('main_content').style.display = "inline-block";
            //        var WorkflowRunStatus = $(this).text();
            //        document.getElementById('h_messasge').innerHTML = WorkflowRunStatus;
            //        document.getElementById('div_body').innerHTML = req.responseText;
            //        $(req.responseText)
            //            .find('StartTime')
            //            .each(function () {
            //                var Starttime = $(this).text();
            //                var inhtml = "";
            //            });
            //        // alert(id);
            //    });
        }

        function OnSuccess(data, status, req, xml, xmlHttpRequest, responseXML) {
            debugger;

            $(req.responseText)
                .find('SessionId')
                .each(function () {
                    debugger;
                    var sessionid = $(this).text();

                    var msgnew = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsh="http://www.informatica.com/wsh">' +
                        '<soapenv:Header>' +
                        '<wsh:Context>' +
                        '<SessionId>' + sessionid+'</SessionId>' +
                        '</wsh:Context>' +
                        '</soapenv:Header>' +
                        '<soapenv:Body>' +
                        '<wsh:GetWorkflowDetails>' +
                        '<DIServiceInfo>' +
                        '<DomainName>Domain_IMNC</DomainName>' +
                        '<ServiceName>IMNC_INT_SVC</ServiceName>' +
                        '</DIServiceInfo>' +
                        '<FolderName>' + username +'</FolderName>' +
                        '<WorkflowName>wf_EMAIL_TASK</WorkflowName>' +
                        '<RequestMode>NORMAL</RequestMode>' +
                        '</wsh:GetWorkflowDetails>' +
                        '</soapenv:Body>' +
                        '</soapenv:Envelope>'
                    $.ajax({
                        url: webServiceURL,
                        headers: {
                            'SOAPAction': '',
                            'Content-Type': 'text/xml'
                        },
                        type: "POST",
                        contentType: "text/xml",
                        dataType: "xml",
                        data: msgnew,
                        success: OnSuccess2,
                        error: OnError2
                    });

                    // alert(id);
                });

        }

        function OnError(request, status, error) {
            debugger;


            alert('error occured');
        }

        $(document).ready(function () {

            $("#btn_click").click(function () {
                CallService();

            });
            jQuery.support.cors = true;
        });

    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };
})();